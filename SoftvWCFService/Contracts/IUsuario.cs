﻿
using AppWCFService.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace AppWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IUsuario
    {

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetAutentificaUsuario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        usuarioEntity GetAutentificaUsuario();


        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "autorizacion", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        AccessEntity autorizacion();



        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "autorizacionmultiple", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        List<AccessEntity> autorizacionmultiple();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "pruebahttps", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        string pruebahttps();



    }
}

