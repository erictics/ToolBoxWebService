﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Web;

namespace AppWCFService.Entity
{
    [DataContract(Namespace = "")]
    public class usuarioEntity
    {     
        [DataMember]
        public string subscriber_id { get; set; }
        [DataMember]
        public string country_code { get; set; }
    }
}