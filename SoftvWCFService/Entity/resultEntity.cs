﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Web;

namespace AppWCFService.Entity
{

    [DataContract(Namespace = "")]
    public class AccessEntity
    {
        [DataMember]
        public bool access { get; set; }


        [DataMember(EmitDefaultValue = false)]
        public string rating { get; set; }

        [DataMember]
        public string urn { get; set; }



        [DataMember(EmitDefaultValue = false)]
        public int ttl { get; set; }

        [DataMember]
        public bool idp_access { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public error error { get; set; }
    }

    [DataContract(Namespace = "")]
    public class AccessMultipleEntity
    {
        [DataMember]
        public bool access { get; set; }

        [DataMember]
        public string rating { get; set; }

        [DataMember]
        public int ttl { get; set; }

        [DataMember]
        public string urn { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public error error { get; set; }
    }


    public class error
    {
        public string errorCode { get; set; }
        public string details { get; set; }
    }
}