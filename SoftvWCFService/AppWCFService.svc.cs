﻿
using Microsoft.IdentityModel.Tokens;
using AppWCFService.Contracts;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Security.Claims;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Xml.Linq;
using AppWCFService.Entity;
using AppWCFService.Services;
using System.Data.SqlClient;
using System.Data;

namespace AppWCFService
{
    [ScriptService]
    public partial class AppWCFService : IUsuario
    {



        #region   
      
        public usuarioEntity GetAutentificaUsuario()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            dbHelper db = new dbHelper();
            try
            {
                var r = HttpContext.Current.Request;
                usuarioEntity user = new usuarioEntity();
                var email = r.Params["email"];
                var password = r.Params["password"];
                int codigo = 0;              
                
                db.agregarParametro("@email", SqlDbType.VarChar, email);
                db.agregarParametro("@password", SqlDbType.VarChar, password);
                SqlDataReader reader = db.consultaReader("Authenticate");
                while (reader.Read())
                {
                    user.subscriber_id = reader[0].ToString();
                    user.country_code = reader[1].ToString();
                    codigo = Int32.Parse(reader[5].ToString());
                  
                }
                db.conexion.Close();
               // user.token = tokenService.GenerateToken("softv");
                if (codigo == 0)
                {
                    error er = new error();
                    er.errorCode = "4";
                    er.details = "Usuario o contraseña inválido";

                    AccessEntity access = new AccessEntity();
                    access.access = false;
                    access.rating = "G";
                    access.ttl = 3600;
                    access.error = er;
                    throw new WebFaultException<AccessEntity>(access, HttpStatusCode.Accepted);
                }
                return user;

            }
            catch (Exception ex)
            {
                db.conexion.Close();
                error er = new error();
                er.errorCode = "4";
                er.details = "Usuario o contraseña inválido";

                AccessEntity access = new AccessEntity();
                access.access = false;
                access.rating = "G";
                access.ttl = 3600;
                access.error = er;
                throw new WebFaultException<AccessEntity>(access, HttpStatusCode.Accepted);
            }



        }

        public bool IsValidSuscriberId(string suscriber_id)
        {
            dbHelper db = new dbHelper();
            bool esvalido = false;
            try
            {
                db.agregarParametro("@suscriber_id", SqlDbType.VarChar, suscriber_id);
                db.agregarParametro("@country_code", SqlDbType.VarChar, "");
                db.agregarParametro("@resource_id", SqlDbType.VarChar, "");
                db.agregarParametro("@autorizacion", SqlDbType.Bit, ParameterDirection.Output);
                db.agregarParametro("@op", SqlDbType.Int, 1);
                db.consultaOutput("validaUsuario");
                esvalido = bool.Parse(db.diccionarioOutput["@autorizacion"].ToString());              
            }
            catch
            {
                db.conexion.Close();
            }
           
            return esvalido;

        }

        public bool IsValidCountryCode(string suscriber_id,string country_code)
        {
            dbHelper db = new dbHelper();
            bool esvalido = false;
            try
            {
                db.agregarParametro("@suscriber_id", SqlDbType.VarChar, suscriber_id);
                db.agregarParametro("@country_code", SqlDbType.VarChar, country_code);
                db.agregarParametro("@resource_id", SqlDbType.VarChar, "");
                db.agregarParametro("@autorizacion", SqlDbType.Bit, ParameterDirection.Output);
                db.agregarParametro("@op", SqlDbType.Int, 2);
                db.consultaOutput("validaUsuario");
                esvalido = bool.Parse(db.diccionarioOutput["@autorizacion"].ToString());
               
            }
            catch(Exception ex)
            {
                db.conexion.Close();
            }
            db.limpiarParametros();
            return esvalido;

        }       


        public bool IsValidUrn(Guid suscriber_id, string urn)
        {
            dbHelper db = new dbHelper();
            bool esvalido = false;
            try
            {
                db.agregarParametro("@subscriberId", SqlDbType.UniqueIdentifier, suscriber_id);
                db.agregarParametro("@urn", SqlDbType.VarChar, urn);                
                db.agregarParametro("@valido", SqlDbType.Bit, ParameterDirection.Output);              
                db.consultaOutput("validaURNCliente");
                esvalido = bool.Parse(db.diccionarioOutput["@valido"].ToString());
            }
            catch
            {

                db.conexion.Close();
            }
            db.limpiarParametros();
            return esvalido;

        }


        public AccessEntity autorizacion()
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                var r = HttpContext.Current.Request;
                string resource_id = r.Params["resource_id"].ToString();
                string suscriber_id = r.Params["subscriber_id"];
                string country_code = r.Params["country_code"];
                string action_id = r.Params["action_id"];
                string ip_address = r.Params["ip_address"];


                //string suscriber_id = HttpContext.Current.Request.QueryString[0];
                //string country_code = HttpContext.Current.Request.QueryString[1];
                //string resource_id = HttpContext.Current.Request.QueryString[2];
                //string action_id = HttpContext.Current.Request.QueryString[3];
                //string ip_address = HttpContext.Current.Request.QueryString[4];
                AccessEntity ae = new AccessEntity();


                Guid subscriber_id = Guid.Empty;
                try
                {
                    subscriber_id = Guid.Parse(suscriber_id);
                }
                catch
                {
                    ae.access = false;
                    ae.rating = "G";
                    ae.ttl = 3600;
                    error e = new error();
                    e.errorCode = "1";
                    e.details = "El usuario no tiene los permisos";
                    ae.error = e;
                }

                if (IsValidSuscriberId(suscriber_id))
                {

                    if (IsValidCountryCode(suscriber_id, country_code))
                    {

                        

                      if (IsValidUrn(subscriber_id, resource_id))
                       {
                            ae.access = true;
                            ae.urn = resource_id;
                            ae.rating = "G";
                            ae.ttl = 86400;
                            ae.idp_access = true;

                        }
                        else
                        {

                            ae.access = false;
                            ae.urn = resource_id;
                            ae.idp_access = false;

                            //ae.access = false;
                            //ae.rating = "G";
                            //ae.ttl = 3600;
                            //ae.urn = resource_id;
                            //error e = new error();
                            //e.errorCode = "3";
                            //e.details = "Resourse_id incorrecto";
                            //ae.error = e;


                        }


                    }
                    else
                    {
                        ae.access = false;
                        ae.rating = "G";
                        ae.ttl = 3600;
                        error e = new error();
                        e.errorCode = "2";
                        e.details = "Country_code inválido";
                        ae.error = e;

                    }

                }
                else
                {
                    ae.access = false;
                    ae.rating = "G";
                    ae.ttl = 3600;
                    error e = new error();
                    e.errorCode = "1";
                    e.details = "El usuario no tiene los permisos";
                    ae.error = e;

                }
                return ae;
            }
            catch (Exception ex)
            {
                AccessEntity ae = new AccessEntity();
                ae.access = false;
                ae.rating = "G";
                ae.ttl = 3600;
                error e = new error();
                e.errorCode = "1";
                e.details = "El usuario no tiene los permisos";
                ae.error = e;
                throw new WebFaultException<AccessEntity>(ae, HttpStatusCode.OK);
            }

        }


        public List<AccessEntity> autorizacionmultiple()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;

            var r = HttpContext.Current.Request;
            List<string> resources = r.Params["resource_id"].ToString().Split('|').ToList();
            var subscriber_id = r.Params["subscriber_id"];
            var country_code = r.Params["country_code"];
            var action_id = r.Params["action_id"];

            Guid suscriber_id = Guid.Empty;
            try
            {
                suscriber_id = Guid.Parse(subscriber_id);
            }
            catch
            {
                List<AccessEntity> listerr = new List<AccessEntity>();
                resources.ForEach(resource => {
                    AccessEntity ae = new AccessEntity();
                    ae.access = false;
                    ae.rating = "G";
                    ae.ttl = 3600;
                    error e = new error();
                    e.errorCode = "1";
                    e.details = "El usuario no tiene los permisos";
                    ae.error = e;
                    
                    listerr.Add(ae);
                });
                throw new WebFaultException<List<AccessEntity>>(listerr, HttpStatusCode.OK);
            }


            if (!IsValidSuscriberId(subscriber_id))
            {
                List<AccessEntity> listerr = new List<AccessEntity>();
                resources.ForEach(resource => {
                    AccessEntity ae = new AccessEntity();
                    ae.access = false;
                    ae.rating = "G";
                    ae.ttl = 3600;
                    error e = new error();
                    e.errorCode = "1";
                    e.details = "El usuario no tiene los permisos";
                    ae.error = e;

                    listerr.Add(ae);
                });
                throw new WebFaultException<List<AccessEntity>>(listerr, HttpStatusCode.OK);
            }

            if (!IsValidCountryCode(subscriber_id, country_code))
            {
                List<AccessEntity> listerr = new List<AccessEntity>();
                resources.ForEach(resource => {

                    AccessEntity ae = new AccessEntity();
                    ae.access = false;
                    ae.rating = "G";
                    ae.ttl = 3600;
                    error e = new error();
                    e.errorCode = "2";
                    e.details = "Country_code inválido";
                    ae.error = e;                    
                    listerr.Add(ae);
                });
                throw new WebFaultException<List<AccessEntity>>(listerr, HttpStatusCode.Accepted);
            }

            List<AccessEntity> lista = new List<AccessEntity>();
            resources.ForEach(resource => {


                if (IsValidUrn(suscriber_id, resource))
                {
                    AccessEntity ae = new AccessEntity();
                    ae.access = true;
                    ae.rating = "G";
                    ae.ttl = 86400;
                    ae.urn = resource;
                    lista.Add(ae);
                }
                else
                {                    
                    AccessEntity ae = new AccessEntity();
                    ae.access = false;
                    ae.urn = resource;
                    ae.idp_access = false;


                    //ae.access = false;
                    //ae.rating = "G";
                    //ae.urn = resource;
                    //ae.ttl = 3600;
                    //error e = new error();
                    //e.errorCode = "3";
                    //e.details = "Resourse_id incorrecto";
                    //ae.error = e;


                    lista.Add(ae);
                }

             });

            return lista;            
        }

       



        public string pruebahttps()
        {
            return "prueba get en https";
        }





        #endregion


    }
}
