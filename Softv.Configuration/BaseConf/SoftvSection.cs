﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftvConfiguration
{
    public class SoftvSection : ConfigurationSection
    {
         /// <summary>
        /// Gets default connection String. If it doesn't exist then
        /// returns global connection string
        /// </summary>
        [ConfigurationProperty("DefaultConnectionString")]
        public String ConnectionString
        {
            get
            {
                string connectionString = (string)base["DefaultConnectionString"];
                connectionString = String.IsNullOrEmpty(connectionString) ?
                    Globals.DataAccess.GlobalConectionString :
                    (string)base["DefaultConnectionString"];
                return connectionString;
            }
        }

        /// <summary>
        /// Gets default assembly name for TestSimulator data clases
        /// </summary>
        [ConfigurationProperty("DefaultAssembly", DefaultValue = "Softv.SQL")]
        public String Assembly
        {
            get { return (string)base["DefaultAssembly"]; }
        }
        /// <summary>
        /// Gets Usuario configuration data
        /// </summary>
        [ConfigurationProperty("Usuario")]
        public UsuarioElement Usuario
        {
            get { return (UsuarioElement)base["Usuario"]; }
        }

       

        /// <summary>
        /// Gets SessionWeb configuration data
        /// </summary>
        [ConfigurationProperty("SessionWeb")]
        public SessionWebElement SessionWeb
        {
            get { return (SessionWebElement)base["SessionWeb"]; }
        }


      
  
    }
}
